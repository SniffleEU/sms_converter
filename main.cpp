#include <QCoreApplication>
#include <QtXml>
#include <QTextStream>
#include <QFileInfo>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString type;
    QString address;
    QString date;
    QString read;
    QString status;
    QString body;
    QString protocol;
    int counter;


    QFile importertsms("200309.xml");
    if(!importertsms.open(QFile::ReadOnly)){
            qDebug() << "Cannot read file" << importertsms.errorString();
            exit(0);
        }

    QXmlStreamReader reader(&importertsms);

    QString filename="exportedsms.json";
    QFile exportedsms( filename );
    if ( exportedsms.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &exportedsms );
        stream << "{" << endl;
        stream << "\t\"messageCount\": 13398," << endl;
        stream << "\t\"messages\": [" << endl;

    if (reader.readNextStartElement()) {
            if (reader.name() == "smses"){
                while(reader.readNextStartElement()){
                    if(reader.name() == "sms"){
                        stream << "\t\t{" << endl;
                        foreach(const QXmlStreamAttribute &attr, reader.attributes()) {

                                if (attr.name().toString() == QLatin1String("type")) {
                                    type = attr.value().toString();
                                }
                                if(attr.name().toString() == QLatin1String("address")){
                                    address = attr.value().toString();
                                }
                                if(attr.name().toString() == QLatin1String("date")){
                                   date = attr.value().toString();
                                }
                                if(attr.name().toString() == QLatin1String("read")){
                                    if (attr.value().toString() == "1"){
                                        read = "true";
                                    }else {
                                        read = "false";
                                    }
                                }
                                if(attr.name().toString() == QLatin1String("status")){
                                    status = attr.value().toString();
                                }
                                if(attr.name().toString() == QLatin1String("body")){
                                    body = attr.value().toString();
                                }
                                if(attr.name().toString() == QLatin1String("protocol")){
                                    protocol = attr.value().toString();
                                }

                            }
                        stream << "\t\t\t\"type\": " << type << "," << endl;
                        stream << "\t\t\t\"address\": \"" << address << "\"," << endl;;
                        stream << "\t\t\t\"date\": " << date << "," << endl;
                        //NY VARIABEL I QKSMS GIR DEFAULT VERDIO OG HÅPER PÅ DET BESTE
                        //dateSent : 0
                        stream << "\t\t\t\"dateSent\": 0," << endl;
                        //NY VARIABEL I QKSMS GIR DEFAULT VERDIO OG HÅPER PÅ DET BESTE
                        stream << "\t\t\t\"read\": " << read << "," << endl;
                        stream << "\t\t\t\"status\": " << status << "," << endl;
                        stream << "\t\t\t\"body\": \"" << body << "\"," << endl;
                        stream << "\t\t\t\"protocol\": " << protocol << "," << endl;
                        //NY VARIABEL I QKSMS GIR DEFAULT VERDIO OG HÅPER PÅ DET BESTE
                        //locked : false
                        stream << "\t\t\t\"locked\": false," << endl;
                        //NY VARIABEL I QKSMS GIR DEFAULT VERDIO OG HÅPER PÅ DET BESTE
                        //subId : -1
                        stream << "\t\t\t\"subId\": -1" << endl;
                        //NY VARIABEL I QKSMS GIR DEFAULT VERDIO OG HÅPER PÅ DET BESTE
                        stream << "\t\t}," << endl;
                        counter += 1;
                        reader.readNext();
            }
            else
                reader.raiseError(QObject::tr("Incorrect file"));
        }

        qDebug() << counter << endl;
      }
    }
        }
    return 0;
}

